FROM fedora:34

LABEL maintainer="Cnes Taro <goldenfishtiger@gmail.com>"

ARG http_proxy
ARG https_proxy

RUN set -x \
	&& dnf install -y \
		httpd \
		diffutils \
	&& rm -rf /var/cache/dnf/* \
	&& dnf clean all

ARG TARGET=httpd
RUN set -x \
	&& /usr/sbin/useradd -m user1
COPY ${TARGET} /home/user1/${TARGET}
RUN set -x \
	&& /usr/bin/chown -R user1:user1 /home/user1

RUN set -x \
	&& mv /etc/httpd/conf/httpd.conf /etc/httpd/conf/httpd.conf.org \
	&& cat /etc/httpd/conf/httpd.conf.org \
		| sed 's/^Listen 80/Listen 8080/' \
		| sed 's/^User apache/User user1/' \
		| sed 's/^Group apache/Group user1/' \
		> /etc/httpd/conf/httpd.conf \
	&& diff -C 2 /etc/httpd/conf/httpd.conf.org /etc/httpd/conf/httpd.conf \
	|| echo '/etc/httpd/conf/httpd.conf changed.'
RUN set -x \
	&& mv /usr/lib/tmpfiles.d/httpd.conf /usr/lib/tmpfiles.d/httpd.conf.org \
	&& cat /usr/lib/tmpfiles.d/httpd.conf.org \
		| sed 's/0 .* apache/0 user1 user1/' \
		> /usr/lib/tmpfiles.d/httpd.conf \
	&& diff -C 2 /usr/lib/tmpfiles.d/httpd.conf.org /usr/lib/tmpfiles.d/httpd.conf \
	|| echo '/usr/lib/tmpfiles.d/httpd.conf changed.'
RUN set -x \
	&& mv /etc/httpd/conf.d/welcome.conf /etc/httpd/conf.d/welcome.conf.bak \
	&& chown -Rv user1:user1 /var/lib/httpd \
	&& chown -Rv user1:user1 /run/httpd \
	&& chmod -Rv ugo+rwx /run/httpd
RUN set -x \
	&& cd /var/www \
	&& rmdir -v html \
	&& ln -sv /var/lib/httpd/pv/html html \
	&& cd /var/log \
	&& rmdir -v httpd \
	&& ln -sv /var/lib/httpd/pv/log httpd

EXPOSE 8080

USER user1
ENTRYPOINT ["httpd", "-DFOREGROUND"]

